import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RequestService } from './service/request.service';

import { IonicStorageModule } from '@ionic/storage';
import { StorageService } from './service/storage.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    IonicStorageModule.forRoot({
    name: '__mydb',
       driverOrder: ['indexeddb', 'sqlite', 'websql']
    }), 
    AppRoutingModule
  ],
  providers: [
    { 
      provide: RouteReuseStrategy, 
      useClass: IonicRouteStrategy 
    }, 
    StorageService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
