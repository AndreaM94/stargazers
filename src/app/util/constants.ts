/**
 *  @description Constants
*/

export namespace Constants {

    /**
     *  @description API REST url Constants
    */
    export namespace RequestConstant {
        export const BASE_URL: string = 'https://api.github.com';
        export const GET_REPO_URL: string = BASE_URL + '/repos/{owner}/{repo}';
        export const GET_STARGAZERS_URL: string = BASE_URL + '/repos/{owner}/{repo}/stargazers';
    }

    /**
     *  @description API REST messages Constants
    */
    export namespace MessagesConstant {
        export const ALLERT_MESSAGE_OK: string = 'Operazione effettuata con successo.';
        export const ALLERT_GENERIC_MESSAGE_KO: string = "Qualcosa è andato storto.";
        export const ALLERT_MESSAGE_GET_REPO_KO: string = "Impossibile recuperare informazioni sul repository selezionato.";
        export const ALLERT_MESSAGE_GET_STARGAZERS_KO: string = "Impossibile recuperare gli Stargazers del repository selezionato.";
        export const ALLERT_EMPTY_MESSAGE: string = 'Nessun risultato trovato.';
    }

    /**
     *  @description Step 1 operations Constants
    */
    export namespace Step1OperationsConstants {
        export const STEP1_OPE_GET_REPO: string = 'OPEGETREPO';
        export const STEP1_OPE_GET_STARGAZERS: string = 'OPEGETSTARGAZERS';
    }
    
    /**
     *  @description Numeric Constants
    */
    export namespace Numbers {
        export const NUMBER_0: number = 0;
        export const NUMBER_1: number = 1;
        export const NUMBER_2: number = 2;
        export const NUMBER_3: number = 3;
        export const NUMBER_4: number = 4;
        export const NUMBER_5: number = 5;
    }

    /**
     *  @description Tab1 String Constants
    */
    export namespace Tab1StringConstant {
        export const TITLE: string = 'Esplora repository';
        export const VIEW_STARGAZERS_BUTTON_LABEL: string = 'Visualizza Stargazers';
        export const STARGAZERS_LIST_CARD_LABEL: string = 'Stargazers List';
        export const NO_REPO_FOUND: string = 'Nessun repository trovato.';
    }

    /**
     *  @description Tab3 String Constants
    */
    export namespace Tab3StringConstant {
        export const PAGE_TITLE: string = 'Impostazioni';
        export const FORM_TITLE: string = "Inserisci l'owner ed il nome del repository su cui effettuare la ricerca.";
        export const FORM_REPOSITORY_OWNER_LABEL: string = 'Repository Owner Name';
        export const FORM_REPOSITORY_NAME_LABEL: string = 'Repository Name';
        export const FORM_BUTTON_SUBMIT_LABEL: string = 'Salva';
    }

}