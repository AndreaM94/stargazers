export namespace Utils {
    /**
     *  @description loader support variable
    */
    export namespace Validation {
        /**
         *  @description verify if a param is valorized
         *  @param {any} arg
         *  @return {boolean}
        */
        export function isValorized(arg: any): boolean {
            return ( arg !== undefined && arg !== null );
        }

        /**
         *  @description verify if a string is valorized
         *  @param {string} arg
         *  @return {boolean}
        */
        export function isStringValorized(arg: string): boolean {
            return ( arg !== undefined && arg !== null && arg !== '' );
        }
    }
}