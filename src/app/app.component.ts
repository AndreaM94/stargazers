import { Component } from '@angular/core';
import { StorageService } from './service/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(public storageService: StorageService) {}

  async ngOnInit() {
    await this.setDefaultRepo();
  }

  /**
   *  @description sets the default repo's owner and owner's name value on storage
  */
  async setDefaultRepo() {
    await this.storageService.set('repositoryOwnerName', 'ionic-team');
    await this.storageService.set('repositoryName', 'ionic-storage');
  }
}
