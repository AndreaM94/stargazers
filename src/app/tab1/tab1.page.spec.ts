import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { StargazersComponentModule } from '../component/stargazers-component.module';
import { RequestService } from '../service/request.service';
import { StorageService } from '../service/storage.service';
import { Tab1PageModule } from './tab1.module';
import { IonicStorageModule } from "@ionic/storage";

import { Tab1Page } from './tab1.page';
import { RouterTestingModule } from '@angular/router/testing';

describe('Tab1Page', () => {
  let component: Tab1Page;
  let fixture: ComponentFixture<Tab1Page>;
  let service: any;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Tab1Page],
      imports: [IonicModule.forRoot(), 
        StargazersComponentModule, 
        Tab1PageModule, 
        IonicStorageModule.forRoot(),
        RouterTestingModule],
      providers: [StorageService, RequestService]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  beforeEach(inject([RequestService], (requestService) => {
    service = requestService;
  }));

  describe('getIonicStorageRepository', () => {
    it('should return response from API REST call get ionic storage repository', () => {
      let fakeResponse = null;
      let repoName = 'ionic-storage';
      let repoOwner = 'ionic-team';
      let url = component.formatUrl(repoOwner, repoName, 'https://api.github.com/repos/{owner}/{repo}/stargazers');
      service.get(url).subscribe((value) => {
        fakeResponse = value;
      });
      expect(fakeResponse).toBeDefined();
    });
  });
});
