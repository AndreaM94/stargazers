import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';

import { Tab1PageRoutingModule } from './tab1-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RequestService } from '../service/request.service';
import { StorageService } from '../service/storage.service';
import { StargazersComponentModule } from '../component/stargazers-component.module';

@NgModule({
  imports: [
    IonicModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    StargazersComponentModule,
    Tab1PageRoutingModule,
  ],
  declarations: [Tab1Page],
  providers: [RequestService, HttpClientModule, HttpClient, StorageService]
})
export class Tab1PageModule {}
