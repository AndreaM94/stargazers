import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, ViewDidEnter } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { BasePaginatorComponent } from '../component/base-paginator-component';
import { ISetupCallApiRest } from '../model/setup.api.rest.interface';
import { RequestService } from '../service/request.service';
import { StorageService } from '../service/storage.service';
import { Constants } from '../util/constants';
import { Utils } from '../util/utils';
import { SupportCallMethodTab1 } from './support.call.method.tab1';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
/**
 *  @description explore repository tab page class
*/
export class Tab1Page extends BasePaginatorComponent implements OnDestroy, ViewDidEnter {

  title: string = Constants.Tab1StringConstant.TITLE;
  noRepoFound: string = Constants.Tab1StringConstant.NO_REPO_FOUND;

  ovbservableGetStargazers: Observable<any>;
  ovbservableGetRepo: Observable<any>;
  subovbservableGetStargazers: Subscription;
  subovbservableGetRepo: Subscription;

  setup: ISetupCallApiRest;
  operation: string;

  owner: string;
  repoName: string;

  repo: any;
  stargazers: Array<any>;

  constructor(public srequest: RequestService,
    public router: Router,
    public storageService: StorageService,
    public alertController: AlertController) { super(srequest, router, alertController) }

  /**
   *  @description cleans partials array, get data from storage, init the setup for API REST CALL and make the call
  */
  async ionViewDidEnter() {
    this.cleanPartials();
    await this.getOwnerAndRepoName();
    this.init();
    this.call(this.setup);
  }
  
  /**
   *  @description unsubscribe from observables when component is destroyed
  */
  ngOnDestroy() {
    if (Utils.Validation.isValorized(this.ovbservableGetStargazers)) {
      this.subovbservableGetStargazers.unsubscribe();
    };
  }

  /**
   *  @description gets the value of the repo's owner and owner's name from storage
  */
  async getOwnerAndRepoName() {
    this.owner = await this.storageService.get('repositoryOwnerName');
    this.repoName = await this.storageService.get('repositoryName');
  }

  /**
   *  @description sets the setup for a API REST call and the management of the response for operation 'OPEGETREPO'
  */
  private init(): void {
    let info = [
      'ovbservableGetRepo',
      this.formatUrl(this.owner, this.repoName, Constants.RequestConstant.GET_REPO_URL),
      Constants.MessagesConstant.ALLERT_MESSAGE_OK,
      Constants.MessagesConstant.ALLERT_MESSAGE_GET_REPO_KO,
      Constants.MessagesConstant.ALLERT_EMPTY_MESSAGE
    ];
    this.setup = this.getSettingsApiRest(info);
    this.operation = Constants.Step1OperationsConstants.STEP1_OPE_GET_REPO;
  }

  /**
   *  @description replace '{owner}' and '{repo}' strings from url with the values setted up on the storage
   *  @return {string}
  */
  formatUrl(owner, repo, url) {
    let formattedUrl;
    formattedUrl = url.replace('{owner}', owner).replace('{repo}', repo);
    return formattedUrl
  }

  /**
   *  @description fire when the evet is emitted from the repository card in order to get the stargazers list
  */
  emitGetStargazers() {
    this.getStargazers();
  }

  /**
   *  @description sets the setup for a API REST call and the management of the response for operation 'OPEGETSTARGAZERS' and make the call
  */
  getStargazers() {
    let info = [
      'ovbservableGetStargazers',
      this.formatUrl(this.owner, this.repoName, Constants.RequestConstant.GET_STARGAZERS_URL),
      Constants.MessagesConstant.ALLERT_MESSAGE_OK,
      Constants.MessagesConstant.ALLERT_MESSAGE_GET_STARGAZERS_KO,
      Constants.MessagesConstant.ALLERT_EMPTY_MESSAGE
    ];
    this.setup = this.getSettingsApiRest(info);
    this.operation = Constants.Step1OperationsConstants.STEP1_OPE_GET_STARGAZERS;
    this.call(this.setup);
  }

  /**
   *  @description response management after API REST call
   *  @param {any} respose
   *  @param {ISetupCallApiRest} setup
   *  @see BaseComponent#responseManagement(...)
  */
  responseManagement(respose: any, setup: ISetupCallApiRest) {
    let map: Map<string, string> = SupportCallMethodTab1.getMethodMap();
    let method: string = map.get(this.operation);
    this[method](respose);
  }

  /**
   *  @description response management after API REST call with OK status for operation 'OPEGETREPO'
   *  @param {any} res
  */
  private retRepoInfo(res: any): void {
    if (res && res.status && res.status !== 200) {
      this.repo = null;
    } else if (res) {
      this.repo = res;
    } else {
      // for jenkins error build to deploy
    }
  }

  /**
   *  @description response management after API REST call with OK status for operation 'OPEGETSTARGAZERS'
   *  @param {any} res
  */
  private retStargazersList(res: any): void {
    this.stargazers = res;
    this.generatePartial(this.stargazers);
  }
}
