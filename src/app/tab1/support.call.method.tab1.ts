import { Constants } from "../util/constants";

/**
 *  @description support call method of an API REST call in Tab 1
*/
export namespace SupportCallMethodTab1 {
    /**
     *  @description return a map of return method of an API REST call
     *  @return {Map<string, string>}
    */
   export function getMethodMap(): Map<string, string> {
    let ret = new Map<string, string>();

    ret.set(Constants.Step1OperationsConstants.STEP1_OPE_GET_REPO, 'retRepoInfo');
    ret.set(Constants.Step1OperationsConstants.STEP1_OPE_GET_STARGAZERS, 'retStargazersList');

    return ret;
   }
}