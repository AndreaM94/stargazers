import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

/**
 *  @description support service for API Rest
*/

@Injectable()
export class RequestService {

    constructor(private httpClient: HttpClient) {}

    /**
     *  @description HTTP get call
     *  @param {string} url
     *  @return {Observable<any>}
    */
    public get(url: string): Observable<any> {
        let headers = new HttpHeaders({'Content-Type': 'application/vnd.github+json'})
        let options = { headers: headers }
        return this.httpClient.get<any>(url, options);
    }

    /**
     *  @description HTTP post call
     *  @param {string} url
     *  @param {Object} body
     *  @return {Observable<any>}
    */
    public post(url: string, body:string): Observable<any> {
        let headers = new HttpHeaders({'Content-Type': 'application/json;charset=UTF-8'})
        let options = { headers: headers }
        return this.httpClient.post<any>(url, body, options);
    }


}