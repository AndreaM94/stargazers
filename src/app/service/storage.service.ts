import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import { Utils } from "../util/utils";

/**
 *  @description support service for Set and Get Strings
*/

@Injectable()
export class StorageService {

    constructor(private storage: Storage) {}

    /**
     *  @description sets a string from storage
     *  @param {string} key
     *  @param {string} value
    */
    public set(key: string, value: string): Promise<any>{
        return this.storage.set(key, value).then((val) => {return val;});
    }

    /**
     *  @description gets a string in storage
     *  @param {string} key
     *  @return {string}
    */
    public get(key: string): Promise<string> {
        return this.storage.get(key).then((val) => {
            return val
        });
    }


}