/**
 *  @description support API REST call
*/
export interface ISetupCallApiRest {

    observable: string;
    url: string;
    allertMessageOK: string;
    allertMessageKO: string;
    allertMessageEmpty: string;

}