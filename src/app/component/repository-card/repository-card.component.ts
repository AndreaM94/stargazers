import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Constants } from '../../util/constants';

@Component({
  selector: 'app-repository-card',
  templateUrl: './repository-card.component.html',
  styleUrls: ['./repository-card.component.scss'],
})
export class RepositoryCardComponent implements OnInit {
  @Input() data: any;
  @Output() emitGetStargazers: EventEmitter<any> = new EventEmitter<any>();

  buttonLabel: string = Constants.Tab1StringConstant.VIEW_STARGAZERS_BUTTON_LABEL;

  constructor() { }

  ngOnInit() {}

  emitGetStargazersEvent() {
    this.emitGetStargazers.emit();
  }

}
