import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewDidEnter } from '@ionic/angular';
import { Constants } from '../../util/constants';

@Component({
  selector: 'app-stargazer-card',
  templateUrl: './stargazer-card.component.html',
  styleUrls: ['./stargazer-card.component.scss'],
})
export class StargazerCardComponent implements ViewDidEnter {
  
  @Input() stargazersList: any;

  stargazersCardListLabel: string = Constants.Tab1StringConstant.STARGAZERS_LIST_CARD_LABEL;

  constructor() { }

  async ionViewDidEnter() {
    console.log(this.stargazersList, 'stargazer list');
  }

}
