import { BaseComponent } from "./base-component";

export abstract class BasePaginatorComponent extends BaseComponent {
    partial: Array<any>;

    /**
     *  @description clean partial array
    */
    public cleanPartials() {
        this.partial = [];
    }

    /**
     *  @description populate partial array from response
    */
    public generatePartial(body) {
        this.partial = body ? body.slice() : [];
    }
}