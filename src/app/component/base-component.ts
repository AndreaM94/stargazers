import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { ISetupCallApiRest } from "../model/setup.api.rest.interface";
import { RequestService } from "../service/request.service";
import { Constants } from "../util/constants";

/**
 *  @description Abstract class with common methods
 *  @class BaseComponent
*/
export abstract class BaseComponent {

    /**
     *  @description loader support variable
    */
    loading: boolean = false;

    constructor(public srequest: RequestService, 
                public router: Router,
                public alertController: AlertController) {
        this.loading = false;
    }

    /**
     *  @description show the loader
    */
    public showLoader() {
        this.loading = true;
    }

    /**
     *  @description hide the loader
    */
    public hideLoader() {
        this.loading = false;
    }

    /**
     *  @description show alert dialog
    */
    public async showAlert(msg: string, code?: string) {
        const alert = await this.alertController.create({
            header: 'Stargazers App',
            subHeader: code,
            message: msg,
            buttons: ['OK'],
        });
    
        await alert.present();
    }

    /**
     *  @description sets needed info for an API REST call
     *  @param {Array<any>} params
     *  @return {ISetupCallApiRest}
    */
    public getSettingsApiRest(params: any) {
        return {
            observable: params[Constants.Numbers.NUMBER_0],
            url: params[Constants.Numbers.NUMBER_1],
            allertMessageOK: params[Constants.Numbers.NUMBER_2],
            allertMessageKO: params[Constants.Numbers.NUMBER_3],
            allertMessageEmpty: params[Constants.Numbers.NUMBER_4]
        }
    }

    /**
     *  @description generic API REST call
     *  @param {ISetupCallApiRest} setup
    */
    public call(setup: ISetupCallApiRest) {
        this.showLoader();
        /** API REST call [GET] */
        this[setup.observable] = this.srequest.get(setup.url);
        /** Observable subscription */
        this['sub' + setup.observable] = this[setup.observable].subscribe(
            (response) => {
                /** Response management */
                this.responseManagement(response, setup);
                this.hideLoader();
            },
            (error) => {
                this.hideLoader();
                this.showAlert(setup.allertMessageKO, error.status);
                this.responseManagement(error, setup);
            }
        )
    }

    /**
     *  @description management API REST call
     *  @param {any} response
     *  @param {ISetupCallApiRest} setup
    */
    public abstract responseManagement(response:any, setup:ISetupCallApiRest): void;


}