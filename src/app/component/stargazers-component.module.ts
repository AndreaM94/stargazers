import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RepositoryCardComponent } from './repository-card/repository-card.component';
import { StargazerCardComponent } from './stargazer-card/stargazer-card.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule],
  declarations: [RepositoryCardComponent, StargazerCardComponent],
  exports: [RepositoryCardComponent, StargazerCardComponent]
})
export class StargazersComponentModule {}
