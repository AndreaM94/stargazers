import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { StorageService } from '../service/storage.service';
import { Tab3PageModule } from './tab3.module';
import { IonicStorageModule } from "@ionic/storage";

import { Tab3Page } from './tab3.page';
import { RouterTestingModule } from '@angular/router/testing';

describe('Tab3Page', () => {
  let component: Tab3Page;
  let fixture: ComponentFixture<Tab3Page>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Tab3Page],
      imports: [IonicModule.forRoot(), 
        ReactiveFormsModule, 
        Tab3PageModule, 
        IonicStorageModule.forRoot(),
        RouterTestingModule],
      providers: [StorageService]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test form should be invalid if input values are null', () => {
    component.repositoryForm.controls.ownerName.setValue(null);
    component.repositoryForm.controls.repoName.setValue(null);
    expect(component.repositoryForm.valid).toBeFalsy();
  });

  it('test form should be invalid if input values are empty string', () => {
    component.repositoryForm.controls.ownerName.setValue('');
    component.repositoryForm.controls.repoName.setValue('');
    expect(component.repositoryForm.valid).toBeFalsy();
  });

  it('test form should be valid', () => {
    component.repositoryForm.controls.ownerName.setValue('test');
    component.repositoryForm.controls.repoName.setValue('test');
    expect(component.repositoryForm.valid).toBeTruthy();
  });
});
