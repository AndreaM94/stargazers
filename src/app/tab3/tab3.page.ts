import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from '../service/storage.service';
import { Constants } from '../util/constants';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
/**
 *  @description settings tab page class, allows you to set the owner name and the repository name to make the research on the explore page
*/
export class Tab3Page implements OnInit {

  repositoryForm: FormGroup;

  pageTitle: string = Constants.Tab3StringConstant.PAGE_TITLE;
  formTitle: string = Constants.Tab3StringConstant.FORM_TITLE;
  ownerLabel: string = Constants.Tab3StringConstant.FORM_REPOSITORY_OWNER_LABEL;
  nameLabel: string = Constants.Tab3StringConstant.FORM_REPOSITORY_NAME_LABEL;
  buttonLabel: string = Constants.Tab3StringConstant.FORM_BUTTON_SUBMIT_LABEL;

  constructor(public storageService: StorageService,private router: Router) {}

  ngOnInit() {
    this.repositoryForm = new FormGroup({
      ownerName: new FormControl('', [Validators.required]),
      repoName: new FormControl('', [Validators.required]),
    });
  }

  /**
   *  @description sets the repository name and owner name on the storage and redirect on the explore repository page
  */
  async onSubmit() {
    await this.storageService.set('repositoryOwnerName', this.repositoryForm.value.ownerName);
    await this.storageService.set('repositoryName', this.repositoryForm.value.repoName);
    this.router.navigate(['']);
  }

}
