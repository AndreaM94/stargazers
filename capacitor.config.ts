import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'stargazers-app',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
